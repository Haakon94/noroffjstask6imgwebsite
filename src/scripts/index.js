    import '../styles/index.scss';

    var searchQuery = "People";
    var numberOfPictures = "10";
    const apiKey = '563492ad6f917000010000019d9ee8ef91174d1b886a984324188e95';

    // Render images and call created card and append to div id row
    function renderPhotos(photos) {
        $.each(photos, (i, photo) => {
            $("#row").append(createdCard(photo));   
        });
    }

    // Return div html to create card with images
    function createdCard(photo) {
        return `
        <div class="remove col-sm-4" style="18rem;">
        <div class="card h-100">
        <div class="card-img">
        <img class="card-img-top" src="${photo.src.portrait}"/>
        </div>

        <div class="card-body">
        <h5>${photo.photographer}</h5>
        </div>
        </div>
        </div>
        `;
    }

    // Perform ajax call to api
    function getPictures(query, perPage) {
        const apiUrl = `https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=${query}&per_page=${perPage}&page=1`;
        $("#spinner").show();
        $.ajax({
            type: 'GET',
            headers: {
            'Authorization': `Bearer ${apiKey}`
            },      
            url: apiUrl,
        }).done(data => {
        $("#spinner").hide();
            renderPhotos(data.photos);
        });
    }

    // When document is ready call getPictures function to display img
    $(document).ready(function() {
        getPictures(searchQuery, numberOfPictures);
    });

    // jQuery click function which calls getPictures with search parameters
    $('#searchBtn').click(function() {
        var searchInput = $('#searchInput');
        var selectedVal = $("#displayNumberOfPics");
        // Remove the class in html and then do another call to api and render the images searched for 
        $(".remove").remove();
        if(selectedVal != null) {
            getPictures(searchInput.val(), selectedVal.val());
        } else {
            getPictures(searchInput.val(), numberOfPictures);
        }
    });

    // jQuery change function which calls getPictures for more img to load on page
    $("#displayNumberOfPics").change(function() {
        var searchInput = $('#searchInput');
        var selectedVal = $("#displayNumberOfPics");
        // Remove the class and the do another call to api and render the images
        $(".remove").remove();
        if(searchInput != null) {
            getPictures(searchInput.val(), selectedVal.val());
        } else {
            getPictures(searchQuery, selectedVal.val());
        }
    });